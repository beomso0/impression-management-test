const video = document.getElementById('video');
const canvas = document.getElementById('drawing');
const screenSize = 2;
video.width = 720/screenSize;
video.height = 560/screenSize;
canvas.width = 720/screenSize;
canvas.height = 560/screenSize;
let expression; 

Promise.all([
  faceapi.nets.tinyFaceDetector.load('./models'),
  faceapi.nets.faceLandmark68Net.load('./models'),
  faceapi.nets.faceRecognitionNet.load('./models'),
  faceapi.nets.faceExpressionNet.load('./models')
]).then(startVideo)

function startVideo() {
  navigator.getUserMedia(
    { video: {} },
    stream => video.srcObject = stream,
    err => console.error(err)
  )
}

video.addEventListener('play', () => {
  // const canvas = faceapi.createCanvasFromMedia(video)
  // canvas.className = "drawing";
  // document.body.append(canvas)
  const displaySize = { width: video.width, height: video.height }
  faceapi.matchDimensions(canvas, displaySize)
  setInterval(async () => {
    const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions()
    expression = detections[0].expressions
    const resizedDetections = faceapi.resizeResults(detections, displaySize)
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
    // faceapi.draw.drawDetections(canvas, resizedDetections)
    faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
    faceapi.draw.drawFaceExpressions(canvas, resizedDetections)
    console.log(expression);
  }, 100)
})